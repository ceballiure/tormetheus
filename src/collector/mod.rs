use std::borrow::Cow;

use tokio::net::TcpStream;
use torut::control::TorAuthData;
use crate::collector::accounting::{AccountingMetrics, Accounting};

use thiserror::Error;

pub(crate) mod accounting;

#[derive(Clone, Debug)]
pub struct Config {
    pub backends: Vec<BackendConfig>,
}

#[derive(Clone, Debug)]
pub struct BackendConfig {
    pub label: String,
    pub remote_address: String,
    pub password: String,
}

impl BackendConfig {
    pub async fn collect_metrics(&self, acc: AccountingMetrics) -> Result<(), CollectionError> {
        let stream = TcpStream::connect(&self.remote_address).await?;
        let mut c = torut::control::UnauthenticatedConn::new(stream);
        c.authenticate(&TorAuthData::HashedPassword(Cow::from(self.password.clone()))).await?;
        let mut authenticated = c.into_authenticated().await;
        authenticated.set_async_event_handler(Some(|_| {
            async move { Ok(()) }
        }));
    
        let accounting = Accounting::from_tor_control(&mut authenticated).await?;
        acc.report(&accounting);

        Ok(())
    }
}

#[derive(Error, Debug)]
pub enum CollectionError {
    #[error("IO Error")]
    IO(#[from] std::io::Error),
    #[error("torut connection error")]
    Torut(#[from] torut::control::ConnError),
    #[error("Accounting metrics collection error")]
    Accounting(#[from] crate::collector::accounting::ControlError),
}
