use prometheus::core::{Collector, Desc};
use prometheus::{IntGauge, Opts, proto, Registry};
use std::str::FromStr;
use std::convert::TryInto;
use torut::control::{AuthenticatedConn, AsyncEvent, ConnError};
use tokio::io::{AsyncRead, AsyncWrite};
use std::future::Future;
use chrono::prelude::*;
use chrono::ParseError;
use thiserror::Error;

#[derive(Debug)]
pub struct Accounting {
    enabled: bool,
    hibernating: Hibernating,
    bytes: ByteStatus,
    bytes_left: ByteStatus,
    interval: Interval,
}

#[derive(Debug)]
pub struct ByteStatus {
    read: u64,
    write: u64,
}

impl FromStr for ByteStatus {
    type Err = ControlError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut split = s.split(' ');
        let read_bytes = split.next().ok_or(ControlError::MissingData)?;
        let read_bytes = read_bytes.parse::<u64>().map_err(|_| ControlError::InvalidValue(s.to_string()))?;
        let write_bytes = split.next().ok_or(ControlError::MissingData)?;
        let write_bytes = write_bytes.parse::<u64>().map_err(|_| ControlError::InvalidValue(s.to_string()))?;

        Ok(ByteStatus {
            read: read_bytes,
            write: write_bytes,
        })
    }
}

#[derive(Debug)]
pub struct Interval {
    start: chrono::DateTime<chrono::Utc>,
    wake: chrono::DateTime<chrono::Utc>,
    end: chrono::DateTime<chrono::Utc>,
}

#[derive(Error, Debug)]
pub enum ControlError {
    #[error("invalid error")]
    InvalidValue(String),

    #[error("connection error")]
    ConnectionError,

    #[error("missing data")]
    MissingData,

    #[error("invalid date")]
    InvalidDate,
}

impl From<ConnError> for ControlError {
    fn from(_: ConnError) -> Self {
        ControlError::ConnectionError
    }
}

impl From<chrono::ParseError> for ControlError {
    fn from(_: ParseError) -> Self {
        ControlError::InvalidDate
    }
}

impl Accounting {
    pub async fn from_tor_control<
        S: AsyncRead + AsyncWrite + Unpin,
        F: Future<Output=Result<(), ConnError>>,
        H: Fn(AsyncEvent<'static>) -> F
    >(control: &mut AuthenticatedConn<S, H>)
    -> Result<Accounting, ControlError>  {
        let enabled = control.get_info("accounting/enabled").await?;
        let hibernating = control.get_info("accounting/hibernating").await?;
        let raw_bytes = control.get_info("accounting/bytes").await?;
        let left = control.get_info("accounting/bytes-left").await?;
        let start = control.get_info("accounting/interval-start").await?;
        let wake = control.get_info("accounting/interval-wake").await?;
        let end = control.get_info("accounting/interval-end").await?;

        Ok(Accounting{
            enabled: bool_from_str(&enabled)?,
            hibernating: Hibernating::from_str(&hibernating)?,
            bytes: ByteStatus::from_str(&raw_bytes)?,
            bytes_left: ByteStatus::from_str(&left)?,
            interval: Interval {
                start: Utc.datetime_from_str(&start, "%Y-%m-%d %H:%M:%S")?,
                wake: Utc.datetime_from_str(&wake, "%Y-%m-%d %H:%M:%S")?,
                end: Utc.datetime_from_str(&end, "%Y-%m-%d %H:%M:%S")?,
            }
        })
    }
}

fn bool_from_str(input: &str) -> Result<bool, ControlError> {
    match input {
        "0"  => Ok(false),
        "1" => Ok(true),
        e => Err(ControlError::InvalidValue(e.to_string())),
    }
}

#[derive(Clone)]
pub struct AccountingMetrics {
    enabled: Box<FlagGauge>,
    hibernating: Box<IntGauge>,
    bytes_read: Box<IntGauge>,
    bytes_write: Box<IntGauge>,
    bytes_left_read: Box<IntGauge>,
    bytes_left_write: Box<IntGauge>,
}

impl AccountingMetrics {
    pub fn new(registry: &Registry) -> Result<Self, prometheus::Error> {
        let hibernating = IntGauge::with_opts(
            Self::default_options(
                "hibernating",
                "The hibernating field is 0 if we are accepting no data; 1 if we're accepting no new connections, and 2 if we're not hibernating at all"
            )
        )?;

        let bytes_read = IntGauge::with_opts(
            Self::default_options(
                "bytes_read",
                "Bytes read since the start of the interval",
            )
        )?;

        let bytes_write = IntGauge::with_opts(
            Self::default_options(
                "bytes_write",
                "Bytes written since the start of the interval",
            )
        )?;

        let bytes_left_read = IntGauge::with_opts(
            Self::default_options(
                "bytes_left_read",
                "Bytes read since the start of the interval",
            )
        )?;

        let bytes_left_write = IntGauge::with_opts(
            Self::default_options(
                "bytes_left_write",
                "Bytes written since the start of the interval",
            )
        )?;

        let metrics = AccountingMetrics {
            enabled: Box::new(FlagGauge::new("enabled")),
            hibernating: Box::new(hibernating),
            bytes_read: Box::new(bytes_read),
            bytes_write: Box::new(bytes_write),
            bytes_left_read: Box::new(bytes_left_read),
            bytes_left_write: Box::new(bytes_left_write),
        };

        metrics.register_metrics(&registry)?;

        Ok(metrics)
    }

    pub fn report(&self, account: &Accounting) {
        self.enabled.set(account.enabled);
        self.hibernating.set(i64::from(account.hibernating.to_gauge()));
        self.bytes_read.set(account.bytes.read.try_into().unwrap_or(i64::max_value()));
        self.bytes_write.set(account.bytes.write.try_into().unwrap_or(i64::max_value()));
        self.bytes_left_read.set(account.bytes_left.read.try_into().unwrap_or(i64::max_value()));
        self.bytes_left_write.set(account.bytes_left.write.try_into().unwrap_or(i64::max_value()));
    }

    pub fn register_metrics(&self, registry: &Registry) -> Result<(), prometheus::Error> {
        registry.register(self.enabled.clone())?;
        registry.register(self.hibernating.clone())?;
        registry.register(self.bytes_read.clone())?;
        registry.register(self.bytes_write.clone())?;
        registry.register(self.bytes_left_read.clone())?;
        registry.register(self.bytes_left_write.clone())?;

        Ok(())
    }

    fn default_options(name: &str, help: &str) -> Opts {
        let hostname = std::env::var_os("TORMETHEUS_HOSTNAME")
                .and_then(|oss| oss.into_string().ok())
                .unwrap_or_else( || "default".to_string());

        Opts::new(name, help)
            .namespace("accounting")
            .const_label("host", hostname.as_str())
    }

    fn default_flag_options(name: &str) -> Opts {
        Self::default_options(
            "name",
            &format!("Reports 1 if {} is enabled. 0 otherwise", name)
        )
    }
}

fn gauge_flag(name: &str) -> IntGauge {
     IntGauge::with_opts(AccountingMetrics::default_flag_options(name)).unwrap()
}

#[derive(Debug)]
pub enum Hibernating {
    Hard,
    Soft,
    Awake,
}

impl FromStr for Hibernating {
    type Err = ControlError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "hard" => Ok(Hibernating::Hard),
            "soft" => Ok(Hibernating::Soft),
            "awake" => Ok(Hibernating::Awake),
            e => Err(ControlError::InvalidValue(e.to_string())),
        }
    }
}

impl Hibernating {
    pub fn to_gauge(&self) -> u8 {
        match self {
            Hibernating::Hard => 0,
            Hibernating::Soft => 1,
            Hibernating::Awake => 2,
        }
    }
}

#[derive(Clone)]
pub struct FlagGauge {
    inner: IntGauge,
}

impl FlagGauge {
    pub fn new(name: &str) -> FlagGauge {
        FlagGauge {
            inner: gauge_flag(name),
        }
    }

    pub fn set(&self, value: bool) {
        if value {
            self.inner.set(1);
        } else {
            self.inner.set(0);
        }
    }
}

impl Collector for FlagGauge {
    fn desc(&self) -> Vec<&Desc> {
        self.inner.desc()
    }

    fn collect(&self) -> Vec<proto::MetricFamily> {
        self.inner.collect()
    }
}