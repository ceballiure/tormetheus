use prometheus::{Registry, TextEncoder, Encoder};
use tiny_http::{Server, Request, Response};

pub struct HttpServer {
    registry: Registry,
}

pub enum Error {
    CouldNotBindAddress,
}

#[derive(Debug)]
pub enum RequestError {
    CouldNotEncodeMetrics,
    CouldNotUtfEncode,
}

impl HttpServer {
    pub fn new(registry: Registry) -> Self {
        HttpServer {
            registry,
        }
    }

    pub fn run(&self) -> Result<(), Error> {
        let server = Server::http("0.0.0.0:9100")
            .map_err(|e| {
                tracing::error!("Error on server bind: {}", e);
                Error::CouldNotBindAddress
            })?;

        for request in server.incoming_requests() {
            if let Err(e) = self.serve_request(request) {
                eprintln!("Error serving a request: {:?}", e)
            }
        };

        Ok(())
    }

    fn serve_request(&self, request: Request) -> Result<(), RequestError> {
        let mut buffer = vec![];
        let encoder = TextEncoder::new();
        let metric_families = self.registry.gather();
        encoder.encode(&metric_families, &mut buffer)
            .map_err(|_| RequestError::CouldNotEncodeMetrics)?;

        let response = Response::from_string(
            String::from_utf8(buffer)
                .map_err(|_| RequestError::CouldNotUtfEncode)?
        );

        if let Err(e) = request.respond(response) {
            eprintln!("HTTP error: {}", e);
        }

        Ok(())
    }
}