use crate::collector::{BackendConfig, Config};
use prometheus::Registry;
use crate::collector::accounting::AccountingMetrics;

use std::{env::var};

mod collector;
mod server;

#[tokio::main]
async fn main() -> Result<(), prometheus::Error> {
    tracing_subscriber::fmt::init();
    let remote = var("TORMETHEUS_REMOTE_HOST").unwrap_or("localhost:9051".to_string());
    let label = var("TORMETHEUS_REMOTE_LABEL").unwrap_or("local".to_string());
    let password = var("TORMETHEUS_REMOTE_PASSWORD").expect("remote password if present");


    let config = Config {
        backends: vec![BackendConfig {
            label,
            remote_address: remote,
            password,
        }]
    };

    tracing::debug!("Debug config: {:?}", config);

    // Create a Registry and register Counter.
    let registry = Registry::new();
    let quota = AccountingMetrics::new(&registry)?;

    // Spawn http server in background
    let ws = server::HttpServer::new(registry);
    tokio::task::spawn_blocking(move || ws.run());

    let mut interval = tokio::time::interval(std::time::Duration::from_secs(30));
    loop {
        interval.tick().await;
        tracing::debug!("Collecting new metrics");

        for bc in config.backends.iter() {
            if let Err(e) = bc.collect_metrics(quota.clone()).await {
                tracing::error!(
                    err = e.to_string().as_str(),
                    "Errored while collecting metrics"
                )
            }
        }
    }
}
