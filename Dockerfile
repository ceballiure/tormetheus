FROM clux/muslrust
RUN mkdir /source
WORKDIR /source
COPY ./Cargo.toml .
COPY ./Cargo.lock .
COPY ./src/ /source/src/
RUN cargo build --release --bin tormetheus
RUN strip /source/target/x86_64-unknown-linux-musl/release/tormetheus

FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=0 /source/target/x86_64-unknown-linux-musl/release/tormetheus /